class BiddingStrategy:
    def __init__(
        self,
        max_charge_bid,
        min_lmp_decided_by_casio,
        min_charge,
        forcast_lmp_025,
        max_make_me_charge_lmp_given_by_operators,
        min_discharge,
        forcast_lmp_500,
        forcast_lmp_975,
        ra_commitment,
        max_discharge_bid,
        max_lmp_decided_by_casio,
    ):
        self.max_charge_bid = max_charge_bid
        self.min_lmp_decided_by_casio = min_lmp_decided_by_casio
        self.min_charge = min_charge
        self.max_make_me_charge_lmp_given_by_operators = (
            max_make_me_charge_lmp_given_by_operators
        )
        self.min_discharge = min_discharge
        self.forcast_lmp_025 = forcast_lmp_025
        self.forcast_lmp_500 = forcast_lmp_500
        self.forcast_lmp_975 = forcast_lmp_975
        self.ra_commitment = ra_commitment
        self.max_discharge_bid = max_discharge_bid
        self.max_lmp_decided_by_casio = max_lmp_decided_by_casio

    def calculate_discharge_bid(self, t, opt_data):
        # 1枚目
        bid_mw_1 = min(-self.max_charge_bid, -0.03)
        bid_price_1 = self.min_lmp_decided_by_casio

        # 2枚目
        bid_mw_2 = -self.min_charge
        bid_price_2 = max(
            min(
                min(self.forcast_lmp_025),
                self.max_make_me_charge_lmp_given_by_operators,
            ),
            self.min_lmp_decided_by_casio + 1,
        )

        # 3枚目
        bid_mw_3 = self.min_discharge
        bid_price_3 = max(self.forcast_lmp_025[t], self.min_lmp_decided_by_casio + 2)

        # 4枚目
        bid_mw_4 = max(min(opt_data[t] - 0.03, self.max_discharge_bid - 0.03), 0.03)
        bid_price_4 = max(self.forcast_lmp_500[t], self.min_lmp_decided_by_casio + 3)

        # 5枚目
        bid_mw_5 = max(min(opt_data[t] - 0.02, self.max_discharge_bid - 0.02), 0.04)
        bid_price_5 = min(max(self.forcast_lmp_975), self.max_lmp_decided_by_casio - 1)

        # 6枚目
        if self.max_discharge_bid >= self.ra_commitment:
            bid_mw_6 = max(self.max_discharge_bid - 0.01, 0.05)
        else:
            bid_mw_6 = max(
                min(self.max_discharge_bid - 0.01, self.ra_commitment - 0.01), 0.05
            )
        bid_price_6 = self.max_lmp_decided_by_casio

        # 7枚目（RA）
        bid_mw_ra = max(max(self.max_discharge_bid, self.ra_commitment), 0.06)
        bid_price_ra = self.max_lmp_decided_by_casio

        return [
            (bid_mw_1, bid_price_1),
            (bid_mw_2, bid_price_2),
            (bid_mw_3, bid_price_3),
            (bid_mw_4, bid_price_4),
            (bid_mw_5, bid_price_5),
            (bid_mw_6, bid_price_6),
            (bid_mw_ra, bid_price_ra),
        ]

    def calculate_charge_bid(self, t, opt_data):
        # 1枚目
        # 1枚目
        bid_mw_1 = min(-self.max_charge_bid, -0.05)
        bid_price_1 = self.min_lmp_decided_by_casio
        # 2枚目
        bid_mw_2 = min(-opt_data[t] - 0.01, -0.04)
        bid_price_2 = max(
            min(
                min(self.forcast_lmp_025),
                self.max_make_me_charge_lmp_given_by_operators,
            ),
            self.min_lmp_decided_by_casio + 1,
        )
        # 3枚目
        bid_mw_3 = min(-opt_data[t], -0.03)
        bid_price_3 = max(self.forcast_lmp_025[t], self.min_lmp_decided_by_casio + 2)
        # 4枚目
        bid_mw_4 = self.min_charge
        bid_price_4 = max(self.forcast_lmp_500[t], self.min_lmp_decided_by_casio + 3)
        # 5枚目
        bid_mw_5 = self.min_discharge
        bid_price_5 = min(max(self.forcast_lmp_975), self.max_lmp_decided_by_casio - 1)
        # 6枚目
        if self.max_discharge_bid >= self.ra_commitment:
            bid_mw_6 = max(self.max_discharge_bid - 0.01, 0.05)
        else:
            bid_mw_6 = max(
                min(self.max_discharge_bid - 0.01, self.ra_commitment - 0.01), 0.05
            )
        bid_price_6 = self.max_lmp_decided_by_casio
        # 7枚目（RA）
        bid_mw_ra = max(max(self.max_discharge_bid, self.ra_commitment), 0.06)
        bid_price_ra = self.max_lmp_decided_by_casio

        return [
            (bid_mw_1, bid_price_1),
            (bid_mw_2, bid_price_2),
            (bid_mw_3, bid_price_3),
            (bid_mw_4, bid_price_4),
            (bid_mw_5, bid_price_5),
            (bid_mw_6, bid_price_6),
            (bid_mw_ra, bid_price_ra),
        ]
